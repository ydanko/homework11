import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Main{
    public static void main(String[] args) {
        ArrayList<Integer> array1 = new ArrayList<Integer>(Arrays.asList(-2, -3, 6, 3));

        ArrayList<Integer> array2 = new ArrayList<Integer>(Arrays.asList(2, -4, 5, -3));

        ArrayList<Integer> array3 = new ArrayList<>();
        array3.addAll(array1);
        array3.addAll(array2);
        System.out.println(array3);

        int min = Collections.min(array3);
        System.out.println("Minimum is : " + min);
    }
}
